#pragma section simple
typedef short                           simple_def;
#pragma section message
#pragma fieldalign shared2 __message
typedef struct __message
{
   char                            eyecatcher[2];
   simple_def                      simple;
   char                            more_text[256];
} message_def;
#define message_def_Size 260
