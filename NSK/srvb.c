#include <stdio.h>     nolist
#include "configh"     nolist
#include "module1h"    nolist
#include "module2h"

int main(int argc, char **argv) {
   printf("Server B - %s\n", MYAPP_NAME);
   module1_call("Server B");
   module2_call("Server B");
   return 0;
}
